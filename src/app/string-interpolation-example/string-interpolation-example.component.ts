import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-string-interpolation-example',
  templateUrl: './string-interpolation-example.component.html',
  styleUrls: ['./string-interpolation-example.component.css']
})
export class StringInterpolationExampleComponent implements OnInit {
  showName = 'examplePokemon';
  constructor() { }

  ngOnInit() {
  }

}
