import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StringInterpolationExampleComponent } from './string-interpolation-example.component';

describe('StringInterpolationExampleComponent', () => {
  let component: StringInterpolationExampleComponent;
  let fixture: ComponentFixture<StringInterpolationExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StringInterpolationExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StringInterpolationExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
